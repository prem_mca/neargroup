var gulp   = require('gulp'),
	less = require('gulp-less'),
	path = require('path'),
	minifyCSS = require('gulp-minify-css'), // Using a gulp plugin to minify css
	clean = require('gulp-clean');

//Function for error handling exit the buid process
function handleError(err) {
	console.log(err.toString());
	process.exit(1);
}

//Clean the css files 
gulp.task('clean', function () {
	return gulp.src(['./app/assets/css/app.css', './app/assets/css/global.css', './app/assets/css/public.css'], {read: false})
		.pipe(clean());
});

//Compile the Global less files to css
gulp.task('globalless', function() {
	return gulp.src('./app/assets/css/less/global.less')
		.pipe(less().on('error', handleError))
		.pipe(minifyCSS())
		.pipe(gulp.dest('./app/assets/css/'));
});

//Compile the public pages css
gulp.task('publicless', function() {
	return gulp.src('./app/assets/css/less/public/public.less')
		.pipe(less().on('error', handleError))
		.pipe(minifyCSS())
		.pipe(gulp.dest('./app/assets/css/'));
});

//Compile the inner pages css
gulp.task('appless', function() {
	return gulp.src('./app/assets/css/less/app/app.less')
		.pipe(less().on('error', handleError))
		.pipe(minifyCSS())
		.pipe(gulp.dest('./app/assets/css/'));
});

//Define the compile less css files and compress those to 
gulp.task('less', ['clean','globalless', 'publicless', 'appless']);

gulp.task('build', ['less']);

gulp.task('default', ['less']);