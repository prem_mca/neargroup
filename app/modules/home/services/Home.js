app.service('HomeService', ['$http', function($http) {
	this.registerUser = function(opts, callback) {
		var url = APP.service.publicApi.Registration;
		doPost($http, url, opts, function(data) {
            callback(data);
        });
	};

	this.verifyUser = function(opts, callback) {
		var url = APP.service.publicApi.Verification;
		doPost($http, url, opts, function(data) {
            callback(data);
        });
	};

	this.getLocality = function(city, callback) {
		var url = 'app/config/city-locality/' + city + '.json';
		$http.get(url).success(function(data) {
                callback(data)
		});
	};

	this.getUserGroups = function(url, callback) {
		doGet($http, url, function(data) {
			callback(data);
		});
	}
}]);