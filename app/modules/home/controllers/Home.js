app.controller('HomeController', ['$scope', '$state', '$auth', '$timeout','HomeService',
	function($scope, $state, $auth, $timeout, HomeService) {
		$scope.userInterestsArr = APP.interests;
		$scope.currentCityArr = APP.currentCity;
		$scope.jobTypeArr = APP.jobType;

		$scope.wentWrongAtServer = function() {
			$scope.someThingWrong = true;
			$timeout(function(){
				$scope.someThingWrong = false;
			}, 3000);
		};		

		$scope.onGoogleSingIn = function(googleUser) {
			var profile = googleUser.getBasicProfile();
			var userProfile = {};

    		userProfile.osVersion = 0; //Fixed for web now
    		userProfile.imeiNo = '0'; //Fixed for web now
    		userProfile.location = 'India'; //Fixed for web now
    		userProfile.fbUserId = profile.getId();
    		userProfile.dob = '13/09/1991';
    		userProfile.deviceType = 0;
    		userProfile.countryCode = '+91'; //Fixed for web now
    		userProfile.noOfFriends = 0;
    		userProfile.eMail = profile.getEmail();
    		userProfile.verificationCode = '13234435456'; //Fixed for web now
    		userProfile.fbWorkHistory = [];
    		userProfile.pushId = '0';
    		userProfile.invitationCode = '-1'; //Fixed for web now
    		userProfile.locale = 'en_US'; //Fixed for web now
    		userProfile.aboutMe = 'null';
    		userProfile.relationship_status = '0'; //Fixed for web now
    		userProfile.manufacturer = '0';
    		userProfile.id = '2';
    		userProfile.gender = 'MALE';
    		userProfile.fbTimelineUrl = profile.getImageUrl();
    		userProfile.mobileNo = 9999690049; //Fixed for web now
    		userProfile.groups = [{
    			'name': 'Near Group4',
    			'privacy': 'OPEN',
    			'id': "435435566878878"
    		}];
    		userProfile.fbEducationHistory = [];
    		userProfile.firstName = 'First';
    		userProfile.lastName = 'Last';
    		userProfile.deviceModel = '0'; //Fixed for web now
    		userProfile.fullName = profile.getName();
    		userProfile.signupType = 'google.web';
    		userProfile.userPicUrl = profile.getImageUrl();
    		userProfile.coverPhotoUrl = profile.getImageUrl();
    		userProfile.verificationCodeTime = '66586583468686'; //Fixed for web now

			HomeService.registerUser(userProfile, function(data) {
				if(data.data.userTo) {
					localStorage.setItem('currentUser', JSON.stringify(data.data.userTo));
					document.cookie = "currentUser=" + data.data.userTo.id;
					window.location = '/app/';
				} else {
					angular.element($('#regsecondstep')).show();
     				$scope.registeration2Step = true;
    				$scope.firstProfile = data.data;
				}
			});
		};

		//Force user to register
		$scope.getCitiesLocality = function(city) {
			HomeService.getLocality(city, function(data) {
				$scope.localityArray = angular.isArray(data)?data:[];
			});
		};

		//$auth service will do a redirect request to provider oauth end-point
		// after user logged in on the provider page (like facebook),
		//we will have  access token sent to our end-point on proxy (/auth/face)
		//we pass this access token to cerebellum.
		$scope.authenticate = function (provider) {
			$scope.authRequestRunning =  true;
		    $auth.authenticate(provider).then(function (data) {
		    	$scope.authRequestRunning =  false;
		    	var userProfile = {};
		    	if( provider == 'facebook' && data.status == 200 ) {
		    		var fbuser = data.data.profile;
		    		userProfile.osVersion = 0; //Fixed for web now
		    		userProfile.imeiNo = '0'; //Fixed for web now
		    		userProfile.location = 'India'; //Fixed for web now
		    		userProfile.fbUserId = fbuser.id;
		    		userProfile.dob = '13/09/1991';
		    		userProfile.deviceType = 0;
		    		userProfile.countryCode = '+91'; //Fixed for web now
		    		userProfile.noOfFriends = 0;
		    		userProfile.eMail = fbuser.email;
		    		userProfile.verificationCode = '13234435456'; //Fixed for web now
		    		userProfile.fbWorkHistory = [];
		    		userProfile.pushId = '0';
		    		userProfile.invitationCode = '-1'; //Fixed for web now
		    		userProfile.locale = 'en_US'; //Fixed for web now
		    		userProfile.aboutMe = 'null';
		    		userProfile.relationship_status = '0'; //Fixed for web now
		    		userProfile.manufacturer = '0';
		    		userProfile.id = '2';
		    		userProfile.gender = 'MALE';//fbuser.gender;
		    		userProfile.fbTimelineUrl = fbuser.picture.data.url;
		    		userProfile.mobileNo = 9999690049; //Fixed for web now
		    		userProfile.groups = [{
		    			'name': 'Near Group4',
		    			'privacy': 'OPEN',
		    			'id': "435435566878878"
		    		}];
		    		userProfile.fbEducationHistory = [];
		    		userProfile.firstName = fbuser.first_name;
		    		userProfile.lastName = fbuser.last_name?fbuser.last_name:'';
		    		userProfile.deviceModel = '0'; //Fixed for web now
		    		userProfile.fullName = fbuser.name;
		    		userProfile.signupType = 'facebook.web';
		    		userProfile.userPicUrl = fbuser.picture.data.url;
		    		userProfile.coverPhotoUrl = fbuser.picture.data.url;
		    		userProfile.verificationCodeTime = '66586583468686'; //Fixed for web now

		    		//console.log(userProfile);
		    		HomeService.registerUser(userProfile, function(data) {
		    			if(data.data.userTo) {
							localStorage.setItem('currentUser', JSON.stringify(data.data.userTo));
							document.cookie = "currentUser=" + data.data.userTo.id;
							window.location = '/app/';
						} else {
							angular.element($('#regsecondstep')).show();
		     				$scope.registeration2Step = true;
		    				$scope.firstProfile = data.data;
						}
					});
		    	} else {
		    		
		    		var twuser = data.data.profile;
					userProfile.osVersion = 0; //Fixed for web now
		    		userProfile.imeiNo = '0'; //Fixed for web now
		    		userProfile.location = 'India'; //Fixed for web now
		    		userProfile.fbUserId = twuser.id_str;
		    		userProfile.dob = '13/09/1991';
		    		userProfile.deviceType = 0;
		    		userProfile.countryCode = '+91'; //Fixed for web now
		    		userProfile.noOfFriends = 0;
		    		userProfile.eMail = 'xxx@gmail.com';
		    		userProfile.verificationCode = '13234435456'; //Fixed for web now
		    		userProfile.fbWorkHistory = [];
		    		userProfile.pushId = '0';
		    		userProfile.invitationCode = '-1'; //Fixed for web now
		    		userProfile.locale = 'en_US'; //Fixed for web now
		    		userProfile.aboutMe = 'null';
		    		userProfile.relationship_status = '0'; //Fixed for web now
		    		userProfile.manufacturer = '0';
		    		userProfile.id = '2';
		    		userProfile.gender = 'MALE';//fbuser.gender;
		    		userProfile.fbTimelineUrl = twuser.profile_image_url;
		    		userProfile.mobileNo = 9999690049; //Fixed for web now
		    		userProfile.groups = [{
		    			'name': 'Near Group4',
		    			'privacy': 'OPEN',
		    			'id': "435435566878878"
		    		}];
		    		userProfile.fbEducationHistory = [];
		    		userProfile.firstName = 'First';
		    		userProfile.lastName = 'Last';
		    		userProfile.deviceModel = '0'; //Fixed for web now
		    		userProfile.fullName = twuser.name;
		    		userProfile.signupType = 'facebook.web';
		    		userProfile.userPicUrl = twuser.profile_image_url;
		    		userProfile.coverPhotoUrl = twuser.profile_image_url;
		    		userProfile.verificationCodeTime = '66586583468686'; //Fixed for web now

		    		//console.log(userProfile);
		    		HomeService.registerUser(userProfile, function(data) {
		    			if(data.data.userTo) {
							localStorage.setItem('currentUser', JSON.stringify(data.data.userTo));
							document.cookie = "currentUser=" + data.data.userTo.id;
							window.location = '/app/';
						} else {
							angular.element($('#regsecondstep')).show();
		     				$scope.registeration2Step = true;
		    				$scope.firstProfile = data.data;
						}
					});
		    	}
		    	console.log(provider);
		    	console.log("complete data ", data);
		    });
		};

		//Verification step / Second step for registration
		$scope.saveVerification = function() {
			if($scope.firstProfile) {
				$scope.verifyUserGoing = true;
				var profile = $scope.secondProfile;
				opts = {};
				opts.apartment = '';
				opts.city = profile.city;
				opts.industry = profile.jobType;
				opts.interests = [profile.interests];//TODO profile.interests; //Array of inetestes ["Fitness: Cycling"]
				opts.jobType = profile.jobType;
				opts.locality = profile.locality;
				opts.location = profile.city;
				opts.nativeLocation = profile.locality;
				opts.verificationCode = '';
				opts.registrationId = $scope.firstProfile.id;
				HomeService.verifyUser(opts, function(data) {
					console.log("register done ", data);
					$scope.verifyUserGoing = false;
					localStorage.setItem('currentUser', JSON.stringify(data.data));
			            document.cookie = "currentUser=" + data.data.id;
			            window.location = '/app/';
				});
			} else {
				$scope.registeration2Step = false;
			}			
		};

		//Get NearGroup items list for each modules
		$scope.getNearGroupItems = function(type) {
			console.log("function called");
			HomeService.getNearGroupItems(type, function(data) {
				console.log(data);
				$scope.listItems = data;
			});
		}

}]);