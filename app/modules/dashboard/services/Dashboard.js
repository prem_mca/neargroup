app.service('DashboardService', ['$http', function($http) {
	this.getRequest = function(url, callback) {
		doGet($http, url, function(data) {
			callback(data);
		});
	};

	this.getPostCommentFeeds = function(url, callback) {
		doGet($http, url, function(data) {
            callback(data);
        });
	};

	this.createNews = function(opts, callback) {
		var url = APP.service.authApi.createNews;
		doPost($http, url, opts, function(data) {
            callback(data);
        });
	};

	this.createCommentsOnNews = function(opts, callback) {
		var url = APP.service.authApi.createComments;
		doPost($http, url, opts, function(data) {
            callback(data);
        });
	};

	this.likeNews = function(opts, callback) {
		var url = APP.service.authApi.likeNews;
		doPost($http, url, opts, function(data) {
            callback(data);
        });
	};

}]);