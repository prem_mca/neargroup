app.controller('HomeController', ['$rootScope', '$scope', '$state', '$timeout', 'DashboardService',
	function($rootScope, $scope, $state, $timeout, DashboardService) {
		var opts = {};
		var that = this;
		$scope.UserThougthsPost = {};
		$scope.postTypeOptions = APP.PostTypeCategory;
		$scope.postType = $scope.postTypeOptions[0];
		$scope.UserThougthsPost.postTypeCategory = '#thoughts'; //Default category selected for Thoughts & advice

		//Change thoghts & Advice catergory type
		$scope.changeThoughtsCategory = function(type) {
			$scope.UserThougthsPost.postTypeCategory = type;
			console.log($scope.UserThougthsPost);
		};

		$scope.changePostType = function(type) {
			$scope.postType = type;
			console.log(type);
		};

		$scope.openCreateNewsForm = function() {
			alert("Called");
			$scope.createNewsTab = !$scope.createNewsTab;
		};

		$scope.submitPost = function(postType) {
			switch(postType) {
				case 1:
				console.log($scope.UserPost.title);
				console.log($scope.UserPost.description);
					if( !($scope.UserPost.title && $scope.UserPost.description) ) {
						$scope.showErrorMessage('Please enter form tag and description');
						return false;
					};
					$scope.createNews(postType, $scope.UserPost.title, $scope.UserPost.description);
					$scope.UserPost.title = '';
					$scope.UserPost.description = '';
					break;
				case 2: 
				break;
				case 3:
				break;
				case 4:
				break;
				case 5:
				break;
				case 6:
				break;
				default:
				break;
			}
			console.log(postType);
		};

		//Create News by user
		$scope.createNews = function(newsType, newsHeadline, description) {
			//alert("Called");
			var opts = {};
			opts.category = 0;
			opts.city = "Delhi";
			description = "Description";
			groupName = "Thoughts & Advice";
			opts.image = "-1";
			opts.name = $rootScope.currentUser.fullName;
			opts.news = description;
			opts.newsHeadline = newsHeadline;
			opts.newsType = "#"+APP.newTypeKeys[newsType];
			opts.postLevel = "city";
			opts.status = "close";
			opts.commentCount = 0;
			opts.creationTime = 0;
			opts.lastViewTime = 0;
			opts.modifiedTime = 0;
			opts.isAnonymous = false;
			opts.isBookmarked = false;
			opts.isCloseNews = false;
			opts.isLiked = false;
			opts.newsLikeCount = 0;
			opts.newsViewCount = 0;
			opts.userId = $rootScope.currentUser.id;
			DashboardService.createNews(opts, function(data) {
				console.log("Created", data);
			});


		} 
		
		//Create comments on the news 
		this.createCommentsOnNews = function() {
			opts = {};
			opts.comment = "Hello comment by Prem";
			opts.fbID = "105741860725466339540";//TODO WHY ?
			opts.userName = $rootScope.currentUser.fullName;
			opts.commentId = 0; //TODO in case or subcomment
			opts.isSubComment = false; //TODO
			opts.newsId = 877; //TODO
			opts.userID = $rootScope.currentUser.id;
			DashboardService.createComment(opts, function(data) {
				console.log("Comments on the news", data);
			});
		};

		//Like News 
		$scope.likeNews = function(index, news) {
			if(!news.liked) {
				news.liked = true;
				news.newsLikeCount += 1;
				opts = {};
				opts.city = news.city;
				opts.userName = $rootScope.currentUser.fullName;
				opts.newsId = news.id;
				opts.userId = $rootScope.currentUser.id;
				DashboardService.likeNews(opts, function(data) {
					console.log("News liked ", data);
				});
			}
		};


		$scope.showErrorMessage = function(msg) {
			$scope.invalidFormEntries = true;
			$scope.errorMessage = msg;
			$timeout(function() {
				$scope.errorMessage = '';
				$scope.invalidFormEntries = false;
			}, 4000);
		};
}]);