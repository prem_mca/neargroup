app.controller('ProfileController', ['$scope', '$rootScope', '$stateParams', '$state', 'ProfileService',
	function($scope, $rootScope, $stateParams, $state, ProfileService) {
		var userId = $stateParams.userId;
		if(!userId) {
			$state.go('app');
		}
		// if($state.current.name === 'profile') 
		// 	//window.location.reload();
		//Get User profile
		var url = APP.service.authApi.getUserProfile+"/"+$rootScope.currentUser.id+"/"+userId;
		ProfileService.getProfile(url, function(data) {
			$scope.userProfile = data.data;
		});

		//Get User feeds
		var feedurl = APP.service.authApi.getUserNews+"/"+$rootScope.currentUser.id+"/"+userId;
		ProfileService.getUserFeeds(feedurl, function(data) {
			if(data.success) {
				$scope.userFeeds = data.data;
			} else {
				$scope.userFeeds = [];
			}
		});
	}
]);