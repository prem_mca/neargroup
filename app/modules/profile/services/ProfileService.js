app.service("ProfileService", ['$http', function($http) {
	this.getProfile = function(url, callback) {
		doGet($http, url, function(data) {
			_userProfile = data;
			callback(data);
		});
	};

	this.getUserFeeds = function(url, callback) {
		doGet($http, url, function(data) {
			callback(data);
		});
	};

}]); 