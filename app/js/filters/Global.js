app.filter('ngdateformat', function($filter) {
 return function(input) {
		if(input == null){ return ""; } 

		var _date = $filter('date')(new Date(input), 'EEEE, MMMM d, y');

		return _date;
	};
});