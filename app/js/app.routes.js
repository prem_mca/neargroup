'use strict';
//Create name space for the public App
var app = angular.module('DashboardApp', [
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'infinite-scroll',
    'mgcrea.ngStrap',
    'ngAnimate'
]);

/**
*	Config for the public router
*/
app.run(['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
    }
]).config(['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$stateProvider', '$locationProvider', '$urlRouterProvider',
	function($controllerProvider, $compileProvider, $filterProvider, $provide, $stateProvider, $locationProvider, $urlRouterProvider) {
		$locationProvider.html5Mode(true); // Enable HTML 5 HISTORY API
		// lazy controller, directive and service
		app.controller = $controllerProvider.register;
		app.directive  = $compileProvider.directive;
		app.filter     = $filterProvider.register;
		app.factory    = $provide.factory;
		app.service    = $provide.service;
		app.constant   = $provide.constant;
		app.value      = $provide.value;

		$urlRouterProvider.otherwise('/app');

		$stateProvider.state('app', {
			url: '/app',
			templateUrl: '/app/modules/dashboard/views/main.html',
		}).state('profile', {
			url: '/app/profile/:userId',
			templateUrl: '/app/modules/profile/views/profile_view.html',
			controller: 'ProfileController',
			resolve: {
				deps: ['$ocLazyLoad',
	                function ($ocLazyLoad) {
						return $ocLazyLoad.load([
							'/app/modules/profile/services/ProfileService.js',
	                        '/app/modules/profile/controllers/ProfileController.js'
	                    ]);
	                }]
			}
		}).state('directories', {
			url: '/app/directories',
			templateUrl: '/app/modules/profile/views/user_directories.html',
			controller: 'DirectoryController',
			resolve: {
				deps: ['$ocLazyLoad',
	                function ($ocLazyLoad) {
						return $ocLazyLoad.load([
							'/app/modules/profile/services/ProfileService.js',
	                        '/app/modules/profile/controllers/DirectoryController.js'
	                    ]);
	                }]
			}
		})
	}
]);