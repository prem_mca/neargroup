app.directive('thoughtsAdvice', function() {
    return {
    	restrict: 'AE',
        templateUrl: '/app/modules/dashboard/views/thoughts_advice.html'
    };
});

app.directive('carpollNearby', function() {
	return {
    	restrict: 'AE',
        templateUrl: '/app/modules/dashboard/views/carpool.html'
    };
});

app.directive('buysellNearby', function() {
	return {
    	restrict: 'AE',
        templateUrl: '/app/modules/dashboard/views/buy_sell.html'
    };
});

app.directive('offerNearby', function() {
	return {
    	restrict: 'AE',
        templateUrl: '/app/modules/dashboard/views/offer.html'
    };
});

app.directive('activityNearby', function() {
	return {
    	restrict: 'AE',
        templateUrl: '/app/modules/dashboard/views/activity.html'
    };
});

app.directive('jobsNearby', function() {
	return {
    	restrict: 'AE',
        templateUrl: '/app/modules/dashboard/views/jobs.html'
    };
});
