//Directive
app.directive('contentLoader', function() {
    return {
        template: '<div class="col-md-12"><div class="contentloader"><i class="fa fa-circle-o-notch fa-spin"></i></div></div>',
        restrict: 'E'
    };
});

app.directive("contenteditable", function() {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ngModel) {

      function read() {
        ngModel.$setViewValue(element.html());
      }

      ngModel.$render = function() {
        element.html(ngModel.$viewValue || "");
      };

      element.bind("blur keyup change", function() {
        scope.$apply(read);
      });
    }
  };
});