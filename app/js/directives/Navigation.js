//header directive
app.directive('topnav', function() {
    return {
        templateUrl: 'app/partials/private/header.html',
        restrict: 'E'
    };
});

app.directive('sidebar', function() {
    return {
        templateUrl: 'app/partials/private/sidebar.html',
        restrict: 'E'
    };
});
