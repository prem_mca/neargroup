'use strict';
//Create name space for the public App
var app = angular.module('HomeApp', [
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'satellizer'
]);

/**
*	Config for the public router
*/
app.run(['$rootScope', '$state', '$stateParams', '$window',
    function ($rootScope, $state, $stateParams, $window) {
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
    }
]).config(['$authProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$stateProvider', '$locationProvider', '$urlRouterProvider',
	function($authProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $stateProvider, $locationProvider, $urlRouterProvider) {
		$locationProvider.html5Mode(true); // Enable HTML 5 HISTORY API
		// lazy controller, directive and service
		app.controller = $controllerProvider.register;
		app.directive  = $compileProvider.directive;
		app.filter     = $filterProvider.register;
		app.factory    = $provide.factory;
		app.service    = $provide.service;
		app.constant   = $provide.constant;
		app.value      = $provide.value;

		//we are using Satellizer module for oauth
		//$authProvider init
		//there is no client id for twitter. set only for facebook
		$authProvider.facebook({
			clientId: window.FacebookAppId
		});
		
		$urlRouterProvider.otherwise('/');

		$stateProvider.state('home', {
			url: '/',
			templateUrl: '/app/modules/home/views/home.html',
		}).state('login', {
			url: '/home/login',
			templateUrl: '/app/modules/home/views/login.html',
			controller: 'LoginController',
			resolve: {
				deps: ['$ocLazyLoad',
	                function ($ocLazyLoad) {
						return $ocLazyLoad.load([
	                        '/app/modules/home/controllers/Login.js'
	                    ]);
	                }]
			}
		}).state('signup', {
			url: '/home/signup',
			templateUrl: '/app/modules/home/views/signup.html',
			controller: 'RegisterationController',
			resolve: {
				deps: ['$ocLazyLoad',
	                function ($ocLazyLoad) {
						return $ocLazyLoad.load([
	                        '/app/modules/home/controllers/User.js'
	                    ]);
	                }]
			}
		}).state('contact', {
			url: '/contact',
			templateUrl: '/app/modules/home/views/contactus.html',
		}).state('about', {
			url: '/about',
			templateUrl: '/app/modules/home/views/aboutus.html',
		})
	}
]);