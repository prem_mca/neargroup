function onLoad() {
	gapi.load('auth2', function() {
		gapi.auth2.init();
	});
}

function doGet($http, url, callback) {
    $http({
        method: 'GET',
        url: url,
        headers : { 'Content-Type': 'application/json; charset=UTF-8' }
    })
    .success( function(data, status, headers, config) {      
        callback(data);
    })
    .error(function(data, status, headers, config){
    	console.log(status);
    	console.log(headers);
    	console.log("Error");
    });
};

//Define App controller
app.controller('AppController', ['$rootScope', '$scope', '$state', 'DashboardService', 
	function($rootScope, $scope, $state, DashboardService) {
		$rootScope.currentUser = JSON.parse(localStorage.getItem('currentUser'));
		if( !$rootScope.currentUser ) {
			window.location = '/logout';
		}

		//Get current user 
		var url1 = APP.service.authApi.getUserGroups+"/"+$rootScope.currentUser.id;
		DashboardService.getRequest(url1, function(data) {
			console.log("User group ", data);
			for(i=0; i<data.data.length; i++) {
				if(data.data[i].group.groupIconCategory === 'ALL_NEARBY') {
					localStorage.setItem("userAllNearByKey", data.data[i].group.id);
					$rootScope.currentUser.userAllNearByKey = data.data[i].group.id;
				}
				if(data.data[i].group.groupIconCategory === 'LOCALITY') {
					localStorage.setItem("userLocalityKey", data.data[i].group.id);
					$rootScope.currentUser.userLocalityKey = data.data[i].group.id;
				}
			}

			$scope.userId = $rootScope.currentUser.id;
			$scope.feedCode = $rootScope.currentUser.userAllNearByKey; // For all type
			$scope.feedType = 'Delhi'; // For all type
			$scope.itemPerPage = 200; //TODO
			$scope.lastItemTimestamp = 0;
			$scope.calling = false;

			//Get Default feeds for Thoughts & Advice
			$scope.fetchFeeds($scope.lastItemTimestamp);
		});

		$scope.fetchFeeds = function(lastItemTimestamp) {
			var targetUrl = '/api/getNewsByCAC/'+$scope.userId+'/'+$scope.feedCode+'/'+$scope.feedType+'/0/0/'+$scope.itemPerPage+'/'+lastItemTimestamp;
			if(!$scope.calling) {
				$scope.calling = true;
				DashboardService.getPostCommentFeeds(targetUrl, function(data) {
					$scope.calling = false;
					$scope.allFeeds = data.data;
				});
			}
		};

		//Change the Feed type
		$scope.filterFeeds = function(feedCode, feedType) {
			if( !$state.is('app') ) {
				$state.go('app');
			}
			$scope.feedCode = feedCode; 
			$scope.feedType = (feedType=='Delhi')?feedType:$rootScope.currentUser.userAllNearByKey+feedType;
			$scope.calling = false;
			$scope.fetchFeeds(0); 
		};

		$scope.loadMoreFeeds = function() {
			//console.log("Function called");
		}

		$scope.openCommentForm = {
			"title": "Comment",
			"content": "Hello Aside<br />This is a multiline message!"
		};

		//Logout the user from the app
		$scope.logout = function() {
			var user = JSON.parse(localStorage.getItem('currentUser'));
			if(user.userType === 'google.web') {
			var auth2 = gapi.auth2.getAuthInstance();
				auth2.signOut().then(function () {
					localStorage.clear();
					window.location = '/logout';
				});
			} else {
				localStorage.clear();
				window.location = '/logout';
			}
		};
	}
]);