//Common function to call the Get services
function doGet($http, url, callback) {
    $http({
        method: "GET",
        url: url,
        headers : { 'Content-Type': 'application/json; charset=UTF-8' }
    })
    .success( function(data){
        callback(data);
    })
    .error(function(data, status, headers, config){
        if(status === 500) {
            angular.element($('#HomeController')).scope().wentWrongAtServer();
        }

    });
}

//Common function to call the post services
function doPost($http, url, opts, callback){
    $http({
        method: "POST",
        url: url,
        data    : opts,
        headers : { 'Content-Type': 'application/json; charset=UTF-8' }
    })
    .success( function(data){
        callback(data);
    })
    .error(function(data, status, headers, config){
        if(status === 500) {
            angular.element($('#HomeController')).scope().wentWrongAtServer();
        }
    });
}