var _ = require('underscore'),
    moment = require('moment'),
    request = require('request'),
    qs = require('querystring'),
    socialAuth = {};

//These 3 callbacks will receive access token from providers (Google/Facebook/Twitter)
//and send that token  to angular client.
socialAuth.goog = function(req, res) {

}

socialAuth.twit = function (req,res) {
    var twitterKeys = config.Accounts.Twitter;
    var requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
    var accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
    var profileUrl = 'https://api.twitter.com/1.1/users/show.json?screen_name=';

    // Part 1 of 2: Initial request from Satellizer.
    if (!req.body.oauth_token || !req.body.oauth_verifier) {
         //console.log("if  First access totken" +req.body.oauth_token);
        var requestTokenOauth = {
            consumer_key: twitterKeys.consumer_key,
            consumer_secret: twitterKeys.consumer_secret,
            callback: req.body.redirectUri
        };

        // Step 1. Obtain request token for the authorization popup.
        request.post({ url: requestTokenUrl, oauth: requestTokenOauth }, function(err, response, body) {
            var oauthToken = qs.parse(body);

            // Step 2. Send OAuth token back to open the authorization screen.
            res.send(oauthToken);
        });
    } else {
        // Part 2 of 2: Second request after Authorize app is clicked.
        var accessTokenOauth = {
            consumer_key: twitterKeys.consumer_key,
            consumer_secret: twitterKeys.consumer_secret,
            token: req.body.oauth_token,
            verifier: req.body.oauth_verifier
        };

        // Step 3. Exchange oauth token and oauth verifier for access token.
        request.post({ url: accessTokenUrl, oauth: accessTokenOauth }, function(err, response, accessToken) {

            accessToken = qs.parse(accessToken);
            //console.log("Access Token ",accessToken);
            var profileOauth = {
                consumer_key: twitterKeys.consumer_key,
                consumer_secret: twitterKeys.consumer_secret,
                oauth_token: accessToken.oauth_token
            };

            // Step 4. Retrieve profile information about the current user.
            request.get({
                url: profileUrl + accessToken.screen_name,
                oauth: profileOauth,
                json: true
            }, function(err, response, profile) {
                var twitterProfile = {
                    accessToken: accessToken,
                    profile: profile
                };
                //console.log("profile => ",profile)
                //console.log(accessToken)
                return res.send(twitterProfile);
            });
        });
    }
}

socialAuth.face = function(req, res) {
    var facebookKeys = config.Accounts.Facebook;
    var accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token';
    var fields = '?fields=id,email,name,first_name,last_name,picture,birthday,gender,location,about';
    var graphApiUrl = 'https://graph.facebook.com/v2.5/me'+fields;
    var params = {
        code: req.body.code,
        client_id: req.body.clientId,
        client_secret: facebookKeys.secret,
        redirect_uri: req.body.redirectUri
    };

    // Step 1. Exchange authorization code for access token.
    request.get({ url: accessTokenUrl, qs: params, json: true }, function(err, response, accessToken) {
        if(err){
            return res.status(500).send({ message: err });
        }
        if (response.statusCode !== 200) {
            return res.status(500).send({ message: accessToken.error.message });
        }
        // Step 2. Retrieve profile information about the current user.
        request.get({ url: graphApiUrl+'&access_token='+accessToken.access_token, json: true }, function(err, response) {
            //console.log("Access Token => ", accessToken)
            var fbProfile = {
                accessToken: accessToken,
                profile: response.body
            };
            return res.status(200).send(fbProfile);
        });
    });
};

module.exports = socialAuth;
