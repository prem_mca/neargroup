var env = (process.argv[process.argv.length-1]==='prod')?'prod':'dev';

var express = require('express'),
	http = require('http')
	fs = require('fs'),
	request = require('request'),
	_ = require('underscore-node'),
	bodyParser = require('body-parser'),
	cons = require('consolidate'),
	path = require('path'),
	cookieParser = require('cookie-parser'),
    socialAuth = require('./app/middlewares/socialAuth'),
	app = express(),
	config = require('./app/config/config-'+env),
	root = fs.realpathSync('.');

var apiBasicAuth = new Buffer(config.app.name+':'+config.app.password).toString('base64');

//configuring vendor based middlewares
app.use('/bower_components', express.static (__dirname + '/bower_components/'));  //handling the statics - bower components
app.use('/partials', express.static (__dirname + '/partials/'));  //handling the statics - bower components
app.use('/app', express.static(__dirname + '/app/'));  //handling the statics - assets (js, css, images)

app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));

app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

//rendering engine
app.set('views', './');
app.engine('html', cons.underscore);
app.set('view engine', 'html');

app.set('env', env);

app.set('devAPI', config.apiUrl.devAPI); // Enable for Development Server 
app.set('prodAPI', config.apiUrl.prodAPI); //Enable for Production Server 

app.get('/', function (req, res) {
	//console.log(req.cookies['currentUser']);
	if(req.cookies['currentUser']) {
        res.redirect('/app/');
    } else { 
		res.render('index.html', {Accounts: config.Accounts});
	}
});

app.get('/home/*', function (req, res) {
	res.render('index.html', {Accounts: config.Accounts});
});

//Render for auth users only
app.get('/app/*', ensureAuthenticated, function (req, res) {
	res.render('layout.html', {Accounts: config.Accounts});
});

//Api Layer
app.use('/api/*', function (req, res, next) {
	var host = app.get('env') == 'dev' ? app.get('devAPI') : app.get('prodAPI');
	var headers = {
		'Authorization': 'Basic '+ apiBasicAuth,
        "Content-Type": 'application/json',
		"accept-language": req.headers['accept-language'],
		"timezoneoffset": req.headers['timezoneoffset']
	};
	var urlArr = req.originalUrl.split('/');
    urlArr.splice(0, 2);
    var path = urlArr.join('/');
    var method = req.method;
    var uri = host + '/' + path;
    //console.log("Calling Api : "+uri );
    //POST INTERCEPTOR
    if (method == 'POST') {
        if (_.isEmpty(req.files)) {
            request.post({
                url: uri,
                json: req.body,
                headers: headers
            }, function (error, httpResponse, body) {
                if (!error) {
                    res.status(httpResponse.statusCode).send(body);
                }
            });
        } else {
            var formData = req.body;
            var fileObj = _.pairs(req.files);
            for (var i = 0; i < fileObj.length; i++) {
                formData[fileObj[i][1].fieldname] = fs.createReadStream(__dirname + '/uploads/' + fileObj[i][1].name);
            }
            request.post({
                url: uri,
                formData: formData,
                headers: headers
            }, function (error, httpResponse, body) {
                if (!error) {
                    res.status(httpResponse.statusCode).send(body);
                }
            });
        }
    }

    //GET INTERCEPTOR
    if (method == 'GET') {
        request.get({
            url: uri,
            headers: headers
        }, function (error, httpResponse, body) {
            if (!error) {
                res.status(httpResponse.statusCode).send(body);
            } else {
                return res.status(httpResponse.statusCode).send({
                    err: error
                });
            }
        });
    }
});

/** social login call backs / routes start ***/
//These 2 callbacks will receive access token from providers (face/twit)
//and send that token  to angular client.
app.post('/auth/google', socialAuth.goog);
app.post('/auth/facebook', socialAuth.face);
app.post('/auth/twitter', socialAuth.twit);

//Logout the user and redirect to home page
app.get('/logout', function(req, res) {
    res.clearCookie('currentUser');
    res.redirect('/');
});

//UTILITY FUNCTIONS
//validates the session token and redirects appropriately
function ensureAuthenticated(req, res, next) {
    var token = req.cookies['currentUser'];
    if (token) {
        next();
    } else {
        res.clearCookie('currentUser');
        res.redirect('/');
    }
}

//Server is running here
var server = app.listen((env==='prod')?config.prodport:config.devport, function() {
	var host = server.address().address;
	var port = server.address().port;
	console.log(env.charAt(0).toUpperCase() + env.slice(1)+" server is listening at http://%s:%s", host, port);
});
